package controllers

import (
	"crud/config/db"
	"crud/models"
	"fmt"

	"github.com/gofiber/fiber/v2"
)

func GetMonHoc(c *fiber.Ctx) error {
	var monhoc []models.Monhoc
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	db.DBInstance["itis"].Find(&monhoc)
	return c.JSON(monhoc)
}

func CreateMonHoc(c *fiber.Ctx) error {
	var monhoc models.Monhoc
	var checkMonhoc models.Monhoc
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	if getUserLogin().Role == "Admin" {
		if err := c.BodyParser(&checkMonhoc); err != nil {
			return c.Status(500).SendString(err.Error())
		}
		maMh := checkMonhoc.Mamonhoc
		if checkMonhoc.Tenmonhoc == "" || checkMonhoc.Mamonhoc == "" {
			return c.JSON(fiber.Map{
				"message": "Thuộc tính không được để trống",
			})
		}
		db.DBInstance["itis"].Where("mamonhoc=?", maMh).Find(&monhoc)
		if monhoc.Mamonhoc == maMh {
			return c.Status(409).JSON(fiber.Map{
				"message": "Mã môn học đã tồn tại",
			})
		}
		db.DBInstance["itis"].Create(&checkMonhoc)
		return c.JSON(checkMonhoc)
	}
	return c.SendStatus(401)
}

func EditMonHoc(c *fiber.Ctx) error {
	id := c.Params("id")
	var monhoc models.Monhoc
	var checkMonhoc models.Monhoc
	var check models.Monhoc
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	db.DBInstance["itis"].First(&monhoc, id)
	maMh := monhoc.Mamonhoc
	if monhoc.Tenmonhoc == "" {
		return c.Status(404).JSON(fiber.Map{
			"message": "Không tìm thấy môn học",
		})
	}
	if err := c.BodyParser(&checkMonhoc); err != nil {
		c.Status(500).SendString(err.Error())
	}
	fmt.Println(checkMonhoc)
	if getUserLogin().Role == "Admin" {
		db.DBInstance["itis"].Find(&check, "mamonhoc =?", checkMonhoc.Mamonhoc)
		if checkMonhoc.Mamonhoc != maMh && check.Id != 0 {
			return c.Status(409).JSON(fiber.Map{
				"message": "Mã môn học đã tồn tại",
			})
		}
		db.DBInstance["itis"].Model(&monhoc).Updates(checkMonhoc)
		return c.SendStatus(200)
	}
	return c.Status(401).JSON(fiber.Map{
		"message": "Bạn không có quyền sửa",
	})
}

func DeleteMonHoc(c *fiber.Ctx) error {
	id := c.Params("id")
	var monhoc models.Monhoc
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	db.DBInstance["itis"].Find(&monhoc, id)
	if monhoc.Tenmonhoc == "" {
		return c.SendStatus(404) //"message": "Không tìm thấy tài liệu",
	}
	if getUserLogin().Role == "Admin" {
		db.DBInstance["itis"].Delete(&monhoc, id)
		return c.SendStatus(200)
	}
	return c.SendStatus(401) //"message": "Bạn không có quyền xoá",
}
