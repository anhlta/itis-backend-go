package controllers

import (
	"crud/config/db"
	"crud/models"
	"fmt"

	"github.com/gofiber/fiber/v2"
)

func GetDocuments(c *fiber.Ctx) error {
	var docmuments []models.Document
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	db.DBInstance["itis"].Find(&docmuments)
	return c.JSON(docmuments)
}

func CreateDocument(c *fiber.Ctx) error {
	var docmument models.Document
	var user models.User
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	if err := c.BodyParser(&docmument); err != nil {
		return c.Status(500).SendString(err.Error())
	}
	fmt.Println(docmument)
	docmument.Id_nguoitao = getUserLogin().Id
	if docmument.Tentailieu == "" || docmument.Monhoc == 0 || docmument.Linktailieu == "" {
		return c.JSON(fiber.Map{
			"message": "Thuộc tính không được để trống",
		})
	}
	db.DBInstance["itis"].Find(&user, docmument.Id_nguoisohuu)
	if user.Id == 0 {
		return c.Status(404).JSON(fiber.Map{
			"message": "Không tìm thấy người sở hữu",
		})
	}
	db.DBInstance["itis"].Create(&docmument)
	return c.JSON(docmument)
}

func EditDocument(c *fiber.Ctx) error {
	id := c.Params("id")
	var document models.Document
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	db.DBInstance["itis"].First(&document, id)
	if document.Tentailieu == "" {
		return c.Status(404).JSON(fiber.Map{
			"message": "Không tìm thấy tài liệu",
		})
	}
	if err := c.BodyParser(&document); err != nil {
		c.Status(500).SendString(err.Error())
	}
	if getUserLogin().Role == "Admin" || document.Id_nguoitao == getUserLogin().Id {
		document.Id_nguoitao = getUserLogin().Id
		db.DBInstance["itis"].Save(&document)
		return c.Status(200).JSON(document)
	}
	return c.Status(401).JSON(fiber.Map{
		"message": "Bạn không có quyền sửa",
	})
}

func DeleteDocument(c *fiber.Ctx) error {
	id := c.Params("id")
	var document models.Document
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	db.DBInstance["itis"].Find(&document, id)
	if document.Tentailieu == "" {
		return c.SendStatus(404) //"message": "Không tìm thấy tài liệu",
	}
	if getUserLogin().Role == "Admin" || document.Id_nguoitao == getUserLogin().Id {
		db.DBInstance["itis"].Delete(&document, id)
		return c.SendStatus(200)
	}
	return c.SendStatus(401) //"message": "Bạn không có quyền xoá",
}
