package controllers

import (
	"crud/config/db"
	"crud/models"
	"fmt"

	"github.com/gofiber/fiber/v2"
)

func GetNamHoc(c *fiber.Ctx) error {
	var namhoc []models.Namhoc
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	db.DBInstance["itis"].Find(&namhoc)
	return c.JSON(namhoc)
}

func CreateNamHoc(c *fiber.Ctx) error {
	var namhoc models.Namhoc
	var checkNamhoc models.Namhoc
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	if getUserLogin().Role == "Admin" {
		if err := c.BodyParser(&checkNamhoc); err != nil {
			return c.Status(500).SendString(err.Error())
		}
		namH := checkNamhoc.Namhoc
		if checkNamhoc.Namhoc == "" {
			return c.JSON(fiber.Map{
				"message": "Thuộc tính không được để trống",
			})
		}
		db.DBInstance["itis"].Where("namhoc=?", namH).Find(&namhoc)
		if namhoc.Namhoc == namH {
			return c.Status(409).JSON(fiber.Map{
				"message": "Mã môn học đã tồn tại",
			})
		}
		db.DBInstance["itis"].Create(&checkNamhoc)
		return c.JSON(checkNamhoc)
	}
	return c.SendStatus(401)
}

func EditNamHoc(c *fiber.Ctx) error {
	id := c.Params("id")
	var namhoc models.Namhoc
	var checkNamhoc models.Namhoc
	var check models.Namhoc
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	db.DBInstance["itis"].First(&namhoc, id)
	maMh := namhoc.Namhoc
	if namhoc.Namhoc == "" {
		return c.Status(404).JSON(fiber.Map{
			"message": "Không tìm thấy môn học",
		})
	}
	if err := c.BodyParser(&checkNamhoc); err != nil {
		c.Status(500).SendString(err.Error())
	}
	fmt.Println(checkNamhoc)
	if getUserLogin().Role == "Admin" {
		db.DBInstance["itis"].Find(&check, "namhoc =?", checkNamhoc.Namhoc)
		if checkNamhoc.Namhoc != maMh && check.Id != 0 {
			return c.Status(409).JSON(fiber.Map{
				"message": "Mã môn học đã tồn tại",
			})
		}
		db.DBInstance["itis"].Model(&namhoc).Updates(checkNamhoc)
		return c.SendStatus(200)
	}
	return c.Status(401).JSON(fiber.Map{
		"message": "Bạn không có quyền sửa",
	})
}

func DeleteNamHoc(c *fiber.Ctx) error {
	id := c.Params("id")
	var namhoc models.Namhoc
	if getUserLogin().Id == 0 {
		return c.Status(401).JSON(fiber.Map{
			"message": "Chưa đăng nhập",
		})
	}
	db.DBInstance["itis"].Find(&namhoc, id)
	if namhoc.Namhoc == "" {
		return c.SendStatus(404) //"message": "Không tìm thấy tài liệu",
	}
	if getUserLogin().Role == "Admin" {
		db.DBInstance["itis"].Delete(&namhoc, id)
		return c.SendStatus(200)
	}
	return c.SendStatus(401) //"message": "Bạn không có quyền xoá",
}
