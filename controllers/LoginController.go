package controllers

// "time"
// 	"github.com/dgrijalva/jwt-go"
//
import (
	"crud/config/db"
	"crud/input"
	"crud/models"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"golang.org/x/crypto/bcrypt"
)

const SecretKey = "secret"

//var token string
var data = make(map[string]interface{})

// Lấy User đăng nhập
func GetUser(c *fiber.Ctx) error {
	cookie := c.Cookies("jwt")
	token, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(SecretKey), nil
	})
	if err != nil {
		return c.SendStatus(401)
	}
	claims := token.Claims.(*jwt.StandardClaims)
	var user models.User
	db.DBInstance["itis"].Where("id = ?", claims.Issuer).First(&user)
	data["user"] = user
	data["accessToken"] = token.Raw
	return c.JSON(fiber.Map{
		"data": data,
	})
}
func getUserLogin() models.User {
	var userCurrent models.User
	b, err := json.Marshal(data["user"])
	if err != nil {
		fmt.Println(err)
	}
	json.Unmarshal(b, &userCurrent)
	fmt.Println(userCurrent)
	return userCurrent

}

//Đăng nhập
func Login(c *fiber.Ctx) error {
	var data input.Login
	if err := c.BodyParser(&data); err != nil {
		return c.Status(500).SendString(err.Error())
	}
	var user models.User
	db.DBInstance["itis"].Where("username=?", data.Username).First(&user)
	if user.Id == 0 {
		c.Status(fiber.StatusNotFound)
		return c.JSON(fiber.Map{
			"message": "user not found",
		})
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(data.Password)); err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{
			"message": "incorrect password",
		})
	}
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    strconv.Itoa(int(user.Id)),
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(), //1 day
	})

	token, err := claims.SignedString([]byte(SecretKey))
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": "could not login",
		})
	}
	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    token,
		Expires:  time.Now().Add(time.Hour * 24),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)
	db.DBInstance["itis"].Where("id = ?", user.Id).First(&user)
	user.Password = ""
	// var userLogin = make(map[string]interface{})
	// userLogin["user"] = user
	// userLogin["accessToken"] = token
	return c.JSON(fiber.Map{
		"user":        user,
		"accessToken": token,
	})
}

//Đăng xuất
func Logout(c *fiber.Ctx) error {
	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)
	return c.SendStatus(200)
}
func Hello(c *fiber.Ctx) error {
	return c.SendString("Hello")
}

//reset password
func ResetPassword(c *fiber.Ctx) error {
	//var resetpassword input.ResetPassword
	fmt.Println(getUserLogin().Role)
	var user models.User
	id := c.Params("id")
	if getUserLogin().Role == "Admin" {
		db.DBInstance["itis"].First(&user, id)
		maSv := user.MaSV
		if user.Id == 0 {
			return c.SendStatus(404) // ko tìm thấy user
		}
		password, _ := bcrypt.GenerateFromPassword([]byte(maSv), 14)
		db.DBInstance["itis"].Model(&user).Updates(models.User{
			Password: string(password),
		})
		return c.SendStatus(200)
	}
	return c.SendStatus(401) // ko co quyen rs
}

// // Đổi mật khẩu
func DoiMatKhau(c *fiber.Ctx) error {
	var user models.User
	var doimatkhau input.DoiMatKhau
	if getUserLogin().Id == 0 {
		return c.SendStatus(401) // chưa đăng nhập
	}
	id := c.Params("id")
	if strconv.Itoa(int(getUserLogin().Id)) == id {
		if err := c.BodyParser(&doimatkhau); err != nil {
			return c.Status(500).SendString(err.Error())
		}
		db.DBInstance["itis"].First(&user, id)
		passwordMoi, _ := bcrypt.GenerateFromPassword([]byte(doimatkhau.PasswordMoi), 14)
		if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(doimatkhau.PasswordCu)); err != nil {
			return c.Status(200).JSON(fiber.Map{
				"message": "Mật khẩu cũ không chính xác",
			})
		} else {
			db.DBInstance["itis"].Model(&user).Updates(models.User{
				Password: string(passwordMoi),
			})
			return c.SendStatus(200)
		}
	}
	return c.SendStatus(401)
}

// //Thay đổi thông tin cá nhân
func EditInfo(c *fiber.Ctx) error {
	var user models.User
	var changeInfo input.ChangeInfo
	if getUserLogin().Id == 0 {
		return c.SendStatus(401) // chưa đăng nhập
	}
	id := c.Params("id")
	if strconv.Itoa(int(getUserLogin().Id)) == id {
		db.DBInstance["itis"].First(&user, id)
		if user.Id == 0 {
			return c.SendStatus(404) // ko tìm thấy user
		}
		if err := c.BodyParser(&changeInfo); err != nil {
			return c.Status(500).SendString(err.Error())
		}
		db.DBInstance["itis"].Model(&user).Updates(models.User{
			Fullname: changeInfo.Fullname,
			Lop:      changeInfo.Lop, NgaySinh: changeInfo.NgaySinh,
			Gioitinh: changeInfo.Gioitinh, Quequan: changeInfo.Quequan,
			Sodienthoai: changeInfo.Sodienthoai, Email: changeInfo.Email, Linkfb: changeInfo.Linkfb,
			Chucvu: changeInfo.Chucvu,
		})
		return c.SendStatus(200)
	}
	return c.SendStatus(401)
}
