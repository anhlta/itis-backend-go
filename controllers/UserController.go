package controllers

import (
	"crud/config/db"
	"crud/input"
	"crud/models"
	"fmt"
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"golang.org/x/crypto/bcrypt"
)

//Lấy User theo id

func GetUserById(c *fiber.Ctx) error {
	var user models.User
	var userbyid input.UserById
	id := c.Params("id")
	if getUserLogin().Id == 0 {
		return c.SendStatus(401) // chưa đăng nhập
	}
	db.DBInstance["itis"].Find(&user, id)
	if user.Id == 0 {
		return c.SendStatus(404)
	}
	userbyid.Fullname = user.Fullname
	userbyid.Khoa = user.Khoa
	userbyid.MaSV = user.MaSV
	return c.JSON(userbyid)
}

//Lấy list User
func GetUsers(c *fiber.Ctx) error {
	var user []models.User
	if getUserLogin().Id == 0 {
		return c.SendStatus(401) // chưa đăng nhập
	}
	db.DBInstance["itis"].Find(&user)
	for i := range user {
		user[i].Password = ""
	}
	return c.JSON(user)
}

//Tạo User mới
func CreateUser(c *fiber.Ctx) error {
	var user models.User
	if getUserLogin().Role == "Admin" {
		if err := c.BodyParser(&user); err != nil {
			return c.Status(500).SendString(err.Error())
		}
		if user.MaSV == "" || user.Fullname == "" || user.Lop == "" || user.Role == "" || user.Gioitinh == "" || user.Quequan == "" || user.Email == "" || user.Linkfb == "" || user.Sodienthoai == "" || user.NgaySinh.String() == "0001-01-01 00:00:00 +0000 UTC" {
			return c.Status(409).JSON(fiber.Map{
				"message": "Thuộc tính không được để trống",
			})
		}

		db.DBInstance["itis"].Where("maSv=?", user.MaSV).Find(&user)
		if user.Id != 0 {
			return c.Status(200).JSON(fiber.Map{
				"message": "Mã SV đã tồn tại",
			})
		}
		maSV := user.MaSV
		user.Username = maSV
		res := strings.Split(maSV, "D")
		result := strings.Split(res[0], "B")
		user.Khoa = string("D" + result[1])
		password, _ := bcrypt.GenerateFromPassword([]byte(maSV), 14)
		user.Password = string(password)
		db.DBInstance["itis"].Create(&user)
		//return c.JSON(user)
		return c.SendStatus(200)
	}
	return c.SendStatus(401)

}

//Xoá User
func DeleteUser(c *fiber.Ctx) error {
	if getUserLogin().Role == "Admin" {
		id := c.Params("id")
		var user models.User
		db.DBInstance["itis"].Find(&user, id)
		if user.Username == "" {
			return c.SendStatus(404) // ko tìm thấy user
		}
		db.DBInstance["itis"].Delete(&user, id)
		return c.SendStatus(200)
	}
	return c.SendStatus(401)
}

//Sửa User
func EditUser(c *fiber.Ctx) error {
	var user models.User
	var editUser input.EditUser
	var checkUser models.User
	if getUserLogin().Role == "Admin" {
		id := c.Params("id")
		db.DBInstance["itis"].First(&user, id)
		maSv := user.MaSV
		if user.Id == 0 {
			return c.SendStatus(404) // ko tìm thấy user
		}
		if err := c.BodyParser(&editUser); err != nil {
			return c.Status(500).SendString(err.Error())
		}
		db.DBInstance["itis"].Find(&checkUser, "maSv =?", editUser.MaSV)
		fmt.Println(checkUser)
		if editUser.MaSV != maSv && checkUser.Id != 0 {
			return c.Status(200).JSON(fiber.Map{
				"message": "Mã SV đã tồn tại",
			})
		}
		//db.DBInstance["itis"].Save(&user)
		masinhvien := editUser.MaSV
		res := strings.Split(masinhvien, "D")
		result := strings.Split(res[0], "B")
		khoa := string("D" + result[1])
		fmt.Println(khoa)
		db.DBInstance["itis"].Model(&user).Updates(models.User{
			Username: editUser.MaSV, Fullname: editUser.Fullname,
			MaSV: editUser.MaSV,
			Lop:  editUser.Lop, NgaySinh: editUser.NgaySinh,
			Role: editUser.Role, Khoa: khoa, Gioitinh: editUser.Gioitinh, Quequan: editUser.Quequan,
			Sodienthoai: editUser.Sodienthoai, Email: editUser.Email, Linkfb: editUser.Linkfb, Ban1: editUser.Ban1,
			Ban2: editUser.Ban2, Chucvu: editUser.Chucvu,
		})
		//return c.JSON(&user)
		return c.SendStatus(200)
	}
	return c.SendStatus(401)
}

// search user
func SearchInfoUser(c *fiber.Ctx) error {
	var search input.SearchInfoUser
	var user []models.User
	if getUserLogin().Id == 0 {
		return c.SendStatus(401)
	}
	if err := c.QueryParser(&search); err != nil {
		return c.Status(500).SendString(err.Error())
	}
	fmt.Println(search.Ban)
	convertBan, _ := strconv.Atoi(search.Ban)
	if search.MaSV != "" {
		db.DBInstance["itis"].Where("maSv = ?", search.MaSV).Find(&user)
		return c.JSON(user)
	} else {
		if search.Khoa != "" {
			if search.Ban != "" {
				if search.Name != "" {
					fmt.Println("Khoá, ban, tên")
					db.DBInstance["itis"].Where(
						db.DBInstance["itis"].Where(
							"name LIKE ?", "%"+search.Name+"%").Where(db.DBInstance["itis"].Where(
							"ban1 = ?", convertBan).Or("ban2 = ?", convertBan)).Where(
							db.DBInstance["itis"].Where("khoa = ?", search.Khoa))).Find(&user)
				} else {
					fmt.Println("Khoá, ban")
					db.DBInstance["itis"].Where(
						db.DBInstance["itis"].Where(db.DBInstance["itis"].Where(
							"ban1 = ?", convertBan).Or("ban2 = ?", convertBan)).Where(
							db.DBInstance["itis"].Where("khoa = ?", search.Khoa))).Find(&user)
				}
			} else {
				if search.Name != "" {
					fmt.Println("Khoá, tên")
					db.DBInstance["itis"].Where(
						db.DBInstance["itis"].Where(
							"name LIKE ?", "%"+search.Name+"%").Where(
							db.DBInstance["itis"].Where("khoa = ?", search.Khoa))).Find(&user)
				} else {
					fmt.Println("Khoá")
					db.DBInstance["itis"].Where("khoa = ?", search.Khoa).Find(&user)
				}
			}
		} else {
			fmt.Println("Không khoá")
			if search.Ban != "" {
				if search.Name != "" {
					fmt.Println("Ban, tên")
					db.DBInstance["itis"].Where(
						db.DBInstance["itis"].Where(
							"name LIKE ?", "%"+search.Name+"%").Where(db.DBInstance["itis"].Where(
							"ban1 = ?", convertBan).Or("ban2 = ?", convertBan))).Find(&user)
				} else {
					fmt.Println("Ban")
					db.DBInstance["itis"].Where(
						"ban1 = ?", convertBan).Or("ban2 = ?", convertBan).Find(&user)
				}
			} else {
				if search.Name != "" {
					fmt.Println("tên")
					db.DBInstance["itis"].Where(
						"name LIKE ?", "%"+search.Name+"%").Find(&user)
				} else {
					return c.JSON(user)
				}
			}
		}
	}
	return c.JSON(user)
}
