module crud

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/gofiber/fiber/v2 v2.16.0
	github.com/jinzhu/configor v1.2.1
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.12
)
