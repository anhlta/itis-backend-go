package input

import "time"

type ChangeInfo struct {
	Fullname    string    `json:"name" form:"name" gorm:"column:name"`
	Lop         string    `json:"lop" form:"lop" gorm:"column:lop"`
	NgaySinh    time.Time `json:"ngaySinh" form:"ngaySinh" gorm:"column:ngaySinh"`
	Gioitinh    string    `json:"gioitinh" form:"gioitinh" gorm:"column:gioitinh"`
	Quequan     string    `json:"quequan" form:"quequan" gorm:"column:quequan"`
	Sodienthoai string    `json:"sodienthoai" form:"sodienthoai" gorm:"column:sodienthoai"`
	Email       string    `json:"email" form:"email" gorm:"column:email"`
	Linkfb      string    `json:"linkfb" form:"linkfb" gorm:"column:linkfb"`
	Chucvu      string    `json:"chucvu" form:"chucvu" gorm:"column:chucvu"`
}
