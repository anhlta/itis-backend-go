package input

type UserById struct {
	Fullname string `json:"name" form:"name" gorm:"column:name"`
	MaSV     string `json:"maSv" form:"maSv" gorm:"column:maSv"`
	Khoa     string `json:"khoa" form:"khoa" gorm:"column:khoa"`
}
