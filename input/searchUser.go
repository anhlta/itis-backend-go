package input

type SearchUser struct {
	MaSv     string `json:"maSv" form:"maSv" gorm:"column:maSv"`
	Khoa     string `json:"khoa" form:"khoa" gorm:"column:khoa"`
	Fullname string `json:"name" form:"name" gorm:"column:name"`
	Ban      int    `json:"ban" form:"ban"`
	Quequan  string `json:"quequan" form:"quequan" gorm:"column:quequan"`
}
