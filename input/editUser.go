package input

import "time"

type EditUser struct {
	Fullname    string    `json:"name" form:"name" gorm:"column:name"`
	MaSV        string    `json:"maSv" form:"maSv" gorm:"column:maSv"`
	Lop         string    `json:"lop" form:"lop" gorm:"column:lop"`
	NgaySinh    time.Time `json:"ngaySinh" form:"ngaySinh" gorm:"column:ngaySinh"`
	Role        string    `json:"role" form:"role" gorm:"column:role"`
	Gioitinh    string    `json:"gioitinh" form:"gioitinh" gorm:"column:gioitinh"`
	Quequan     string    `json:"quequan" form:"quequan" gorm:"column:quequan"`
	Sodienthoai string    `json:"sodienthoai" form:"sodienthoai" gorm:"column:sodienthoai"`
	Email       string    `json:"email" form:"email" gorm:"column:email"`
	Linkfb      string    `json:"linkfb" form:"linkfb" gorm:"column:linkfb"`
	Ban1        int       `json:"ban1" form:"ban1" gorm:"column:ban1"`
	Ban2        int       `json:"ban2" form:"ban2" gorm:"column:ban2"`
	Chucvu      string    `json:"chucvu" form:"chucvu" gorm:"column:chucvu"`
}
