package input

type SearchInfoUser struct {
	Name string `query:"name"`
	MaSV string `query:"maSv" json:"maSv"`
	Khoa string `query:"khoa"`
	Ban  string `query:"ban"`
}
