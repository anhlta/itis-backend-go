package input

type DoiMatKhau struct {
	PasswordCu  string `json:"matkhaucu" form:"matkhaucu"`
	PasswordMoi string `json:"matkhaumoi" form:"matkhaumoi"`
}
