package db

import (
	"crud/config"
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DBInstance map[string]*gorm.DB

func ConnectSql() map[string]*gorm.DB {
	DBInstance = make(map[string]*gorm.DB)
	for _, connect := range config.Config.Sql {
		dns := connect.Username + ":" + connect.Password + "@tcp(" + connect.Host + ":" + connect.Port + ")/" + connect.Database + "?charset=utf8mb4&parseTime=True&loc=Local"
		db, err := gorm.Open(mysql.Open(dns), &gorm.Config{})
		if err != nil {
			fmt.Println(err.Error())
			return DBInstance
		}
		fmt.Println("Connect Success " + connect.Database + " " + connect.Host + ":" + connect.Port)
		DBInstance[connect.Name] = db
	}
	return DBInstance
}
