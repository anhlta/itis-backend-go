package redisClient

import (
	"crud/config"
	"fmt"
	"strconv"

	"github.com/go-redis/redis"
)

var RedisClient map[string]*redis.Client

func CreateRedisClient() map[string]*redis.Client {
	RedisClient = make(map[string]*redis.Client)
	for _, connect := range config.Config.Redis {
		number, _ := strconv.Atoi(connect.RedisDb)

		client := redis.NewClient(&redis.Options{
			Addr:     connect.Host + ":" + connect.Port,
			Password: connect.Password,
			DB:       number,
		})

		_, err := client.Ping().Result()
		if err != nil {
			return nil
		}
		fmt.Println("=== Connect Redis " + connect.Name + " " + connect.Port + " " + " Success ===")
		RedisClient[connect.Name] = client
	}
	return RedisClient
}
