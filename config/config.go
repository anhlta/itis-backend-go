package config

import (
	"fmt"

	"github.com/jinzhu/configor"
)

type DatabaseDetails struct {
	Name     string
	Host     string
	Port     string
	Username string
	Password string
	Database string
}

type RedisClients struct {
	Name      string
	Host      string
	Port      string
	Password  string
	RedisDb   string
	KeyPrefix string
}

type SmtpClients struct {
	Name               string
	Host               string
	Port               string
	Username           string
	Password           string
	RejectUnauthorized bool
	Timeout            string
}
type Confiurations struct {
	Port  string
	Sql   []DatabaseDetails
	Redis []RedisClients
	Smtp  []SmtpClients
}

var (
	Config   Confiurations
	confPath string
)

func LoadConfig(key string) {
	// if key == "production" {
	// 	confPath = "./config/configSql_pro.yaml"
	// } else {
	// 	confPath = "./config/configSql.yaml"
	// }
	// confPath = "./config/configSql.yaml"
	// _ = configor.New(&configor.Config{Verbose: true}).Load(&Config, confPath)
	confPath = "./config/configSql.yaml"
	err := configor.Load(&Config, confPath)
	if err != nil {
		fmt.Println("Decode error")
		panic(err)
	}
}
