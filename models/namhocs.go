package models

type Namhoc struct {
	Id     uint   `json:"_id" form:"id" gorm:"column:id;primary_key"`
	Namhoc string `json:"namhoc" form:"namhoc" gorm:"column:namhoc"`
}
