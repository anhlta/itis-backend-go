package models

type Monhoc struct {
	Id        uint   `json:"_id" form:"id" gorm:"column:id;primary_key"`
	Tenmonhoc string `json:"tenmonhoc" form:"tenmonhoc" gorm:"column:tenmonhoc"`
	Mamonhoc  string `json:"mamonhoc" form:"mamonhoc" gorm:"column:mamonhoc"`
}
