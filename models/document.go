package models

type Document struct {
	Id            uint   `json:"_id" form:"id" gorm:"column:id;primary_key"`
	Tentailieu    string `json:"tentailieu" form:"tentailieu" gorm:"column:tentailieu"`
	Monhoc        uint   `json:"idmonhoc" form:"idmonhoc" gorm:"column:idmonhoc"`
	Giangvien     string `json:"giangvien" form:"giangvien" gorm:"column:giangvien"`
	Linktailieu   string `json:"linktailieu" form:"linktailieu" gorm:"column:linktailieu"`
	Mota          string `json:"mota" form:"mota" gorm:"column:mota"`
	Id_nguoisohuu uint   `json:"idnguoisohuu" form:"idnguoisohuu" gorm:"column:idnguoisohuu"`
	Id_nguoitao   uint   `json:"idnguoitao" form:"idnguoitao" gorm:"column:idnguoitao"`
}
