package main

import (
	"crud/config"
	"crud/config/db"
	"crud/routers"
	"fmt"
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func main() {
	fmt.Println("========== Start =============")
	app := fiber.New()
	app.Use(func(c *fiber.Ctx) error {
		c.Set("Access-Control-Allow-Origin", "*")
		c.Set("Access-Control-Allow-Methods", "*")
		c.Set("X-XSS-Protection", "1;mode=block")
		c.Set("X-Content-Type-Options", "nosniff")
		c.Set("X-Download-Options", "noopen")
		c.Set("Strict-Transport-Security", "max-age=5184000")
		c.Set("X-Frame-Options", "SAMEORIGIN")
		c.Set("X-DNS-Prefectch-Control", "off")
		c.Set("Content-Type", "application/json")
		c.Set("Access-Control-Allow-Credentials", "true")
		return c.Next()
	})
	app.Use(cors.New(cors.Config{
		AllowOrigins: "http://localhost:8000",
	}))
	config.LoadConfig(os.Getenv("NODE_ENV"))
	db.ConnectSql()
	// redisClient.CreateRedisClient()
	routers.InitRouters(app)
	log.Fatal(app.Listen(":" + config.Config.Port))
}
