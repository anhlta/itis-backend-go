package routers

import (
	"crud/controllers"

	"github.com/gofiber/fiber/v2"
)

func UserRouter(app fiber.Router) {
	app.Get("/hello", controllers.Hello)
	app.Get("/users", controllers.GetUsers)               //lấy danh sách user
	app.Get("/user", controllers.GetUser)                 // lấy user đăng nhập
	app.Get("/user/:id", controllers.GetUserById)         // lấy user theo id
	app.Post("/createUser", controllers.CreateUser)       // thêm user
	app.Delete("/deleteUser/:id", controllers.DeleteUser) //xoá user
	app.Put("/editUser/:id", controllers.EditUser)        // sửa user
	app.Get("/searchUser/", controllers.SearchInfoUser)
}
