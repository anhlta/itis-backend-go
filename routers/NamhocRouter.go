package routers

import (
	"crud/controllers"

	"github.com/gofiber/fiber/v2"
)

func NamHoctRouter(app fiber.Router) {
	app.Get("/listnamhoc", controllers.GetNamHoc)
	app.Post("/create-namhoc", controllers.CreateNamHoc)
	app.Put("/edit-namhoc/:id", controllers.EditNamHoc)
	app.Delete("/delete-namhoc/:id", controllers.DeleteNamHoc)
}
