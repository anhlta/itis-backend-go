package routers

import (
	"crud/controllers"

	"github.com/gofiber/fiber/v2"
)

func LoginRouter(app fiber.Router) {
	app.Post("login", controllers.Login)
	app.Get("user", controllers.GetUser)
	app.Get("", controllers.Hello)
	app.Post("logout", controllers.Logout)
	app.Put("resetpassword/:id", controllers.ResetPassword)
	app.Put("doimatkhau/:id", controllers.DoiMatKhau)
	app.Put("thaydoithongtincanhan/:id", controllers.EditInfo)
}
