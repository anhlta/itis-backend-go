package routers

import (
	"crud/controllers"

	"github.com/gofiber/fiber/v2"
)

func DocumentRouter(app fiber.Router) {
	app.Get("/documents", controllers.GetDocuments)
	app.Post("/create-document", controllers.CreateDocument)
	app.Put("/edit-document/:id", controllers.EditDocument)
	app.Delete("/delete-document/:id", controllers.DeleteDocument)
}
