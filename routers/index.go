package routers

import (
	"github.com/gofiber/fiber/v2"
)

func InitRouters(app *fiber.App) {
	LoginRouter(app.Group("/api"))
	DocumentRouter(app.Group("/api"))
	UserRouter(app.Group("/api"))
	MonHoctRouter(app.Group("/api"))
	NamHoctRouter(app.Group("/api"))
}
