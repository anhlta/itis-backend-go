package routers

import (
	"crud/controllers"

	"github.com/gofiber/fiber/v2"
)

func MonHoctRouter(app fiber.Router) {
	app.Get("/listmonhoc", controllers.GetMonHoc)
	app.Post("/create-monhoc", controllers.CreateMonHoc)
	app.Put("/edit-monhoc/:id", controllers.EditMonHoc)
	app.Delete("/delete-monhoc/:id", controllers.DeleteMonHoc)
}
